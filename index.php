<html>
    <head> 
        <link href="assets/css/bootstrap.min.css" rel="stylesheet">
        <link href="assets/css/octicons.min.css" rel="stylesheet">
        <link href="assets/css/bootstrap-colorpicker.min.css" rel="stylesheet">
        

        <script src="assets/js/jquery-3.1.1.min.js"></script>
        <script src="assets/js/bootstrap.min.js"></script>
        <script src="assets/js/bootstrap-colorpicker.js"></script>
       <?php require 'style.php'; ?>
    </head>
    <body>
        <div class="container">
            <h2>Change Color Scheme </h2>
            <form action="submit.php" method="post">
                <div class="row">
                    <p>Set color of all Paragraphs </p>
                    <div id="cp2" class="input-group colorpicker-component">
                        <input type="text" value="#00AABB" class="form-control" name="paragraph_color" />
                        <span class="input-group-addon"><i></i></span>
                    </div>
                </div>
                <div class="row">
                    <p>Set color of title of the page</p>
                    <div id="cp3" class="input-group colorpicker-component">
                        <input type="text" value="#3a5859" class="form-control" name="title_color"/>
                        <span class="input-group-addon"><i></i></span>
                    </div>
                </div>
                <div class="row">
                    <p>Set the background color of page</p>
                    <input href="#" class="btn btn-default" id="cp4" name="bg_color" />Change background color
                </div>
                <input type="submit" value="Save" />
            </form>
        </div>
       
    <script>
        $(function() {
            $('#cp2').colorpicker();
        });
         $(function() {
            $('#cp3').colorpicker();
        });
        $(function() {
        $('#cp4').colorpicker().on('changeColor', function(e) {
            $('body')[0].style.backgroundColor = e.color.toString(
                'rgba');
        });
        });
    </script>   
    </body>
</html>